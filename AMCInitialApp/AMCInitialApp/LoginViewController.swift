//
//  LoginViewController.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 31/5/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextField: DesignableTextField!
    @IBOutlet weak var passwordTextField: DesignableTextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var notRegisteredYetLabel: UILabel!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    
    // Login
    @IBAction func loginButtonPressed(_ sender: AnyObject) {
        
        print("Validation/verification checks")
        
    }
    
    // Forgot password
    @IBAction func forgotPasswordButtonPressed(_ sender: AnyObject) {
        
        print("Forgot password")
        
    }
    
    // Redirect to register view
    @IBAction func createAccountButtonPressed(_ sender: AnyObject) {
        
        print("Redirect to register viewcontroller")
        
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
