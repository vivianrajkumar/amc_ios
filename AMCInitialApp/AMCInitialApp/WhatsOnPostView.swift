//
//  WhatsOnPostView.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 31/5/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

class WhatsOnPostView: UIView {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var daysAgoLabel: UILabel!
    @IBOutlet weak var readPostButton: UIButton!
        
    @IBAction func readPostButtonPressed(_ sender: AnyObject) {
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
