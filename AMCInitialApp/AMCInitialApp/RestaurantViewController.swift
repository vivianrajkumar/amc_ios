//
//  RestaurantViewController.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 6/6/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

class RestaurantViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var featuredPromotionsScrollView: UIScrollView!
    @IBOutlet weak var featuredPromotionsPageControl: UIPageControl!

    var restaurant: Restaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = restaurant?.title
        
        // Set-up UIScrollView
        featuredPromotionsScrollView.isPagingEnabled = true
        featuredPromotionsScrollView.showsHorizontalScrollIndicator = false
        featuredPromotionsScrollView.delegate = self
        featuredPromotionsPageControl.numberOfPages = (restaurant?.featuredPromotions?.count)!
        featuredPromotionsScrollView.contentSize = CGSize(width: self.view.bounds.width * CGFloat((restaurant?.featuredPromotions?.count)!), height: 230)
        
        loadFeaturedPromotions()
        
    }
    
    // Load restaurant's featured promotions onto UIScrollView
    func loadFeaturedPromotions() {
        
        for (index, promotion) in (restaurant?.featuredPromotions?.enumerated())! {
            
            if let featuredPromotionView = Bundle.main.loadNibNamed("FeaturedPromotion", owner: self, options: nil)?.first as? FeaturedPromotionView {
                
                featuredPromotionView.promotionImage.image = UIImage(named: promotion.imageName!)
                featuredPromotionsScrollView.addSubview(featuredPromotionView)
                featuredPromotionView.frame.size.width = self.view.bounds.size.width
                featuredPromotionView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
                
                let selector = #selector(RestaurantViewController.imageTapped(tapGestureRecognizer:))
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
                featuredPromotionView.promotionImage.isUserInteractionEnabled = true
                featuredPromotionView.promotionImage.addGestureRecognizer(tapGestureRecognizer)
            }
            
        }
        
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        
        // print(featuredPromotionsPageControl.currentPage)
        self.performSegue(withIdentifier: "promotionDetailSegue", sender: self)
        
    }
    
    // Pass post data to WhatsOnDetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination: WhatsOnDetailViewController = segue.destination as! WhatsOnDetailViewController
        destination.promotionPost = restaurant?.featuredPromotions?[featuredPromotionsPageControl.currentPage]
        destination.postType = MenuType.foodBeverage
        
    }
    
    // UIScrollView Paging
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        featuredPromotionsPageControl.currentPage = Int(page)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
