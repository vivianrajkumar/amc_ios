//
//  MenuHeaderView.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 1/6/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

class MenuHeaderView: UIView {
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var welcomeMessageLabel: UILabel!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
