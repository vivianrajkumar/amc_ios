//
//  WhatsOnPost.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 2/6/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import Foundation

class WhatsOnPost: NSObject {
    
    var imageName: String?
    var title: String?
    var desc: String?
    var days: String?
    
}
