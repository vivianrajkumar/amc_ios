//
//  WhatsOnDetailViewController.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 5/6/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

class WhatsOnDetailViewController: UIViewController {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var postDescLabel: UILabel!
    
    var whatsNewPost: WhatsOnPost?
    var promotionPost: Promotion?
    var postType: MenuType?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let type = postType {
        
            switch type {
            case MenuType.whatsOn:
                loadWhatsNewPostDetails()
            case MenuType.foodBeverage:
                loadPromotionPostDetails()
            }
            
        }
        
    }
    
    func loadWhatsNewPostDetails() {

        postImageView.image = UIImage(named: (whatsNewPost?.imageName)!)
        postTitleLabel.text = whatsNewPost?.title
        postDescLabel.text = whatsNewPost?.desc
        postDescLabel.numberOfLines = 0
        postDescLabel.sizeToFit()
                
        // Adjust backView's bottom margin based on postDescLabel height
        let bottomConstraint = NSLayoutConstraint(item: backView, attribute: NSLayoutAttribute.bottomMargin, relatedBy: NSLayoutRelation.equal, toItem: postDescLabel, attribute: NSLayoutAttribute.bottomMargin, multiplier: 1, constant: 20)
        view.addConstraint(bottomConstraint)
        
    }
    
    func loadPromotionPostDetails() {
        
        postImageView.image = UIImage(named: (promotionPost?.imageName)!)
        postTitleLabel.text = promotionPost?.title
        postDescLabel.text = promotionPost?.desc
        postDescLabel.numberOfLines = 0
        postDescLabel.sizeToFit()
        
        // Adjust backView's bottom margin based on postDescLabel height
        let bottomConstraint = NSLayoutConstraint(item: backView, attribute: NSLayoutAttribute.bottomMargin, relatedBy: NSLayoutRelation.equal, toItem: postDescLabel, attribute: NSLayoutAttribute.bottomMargin, multiplier: 1, constant: 20)
        view.addConstraint(bottomConstraint)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
