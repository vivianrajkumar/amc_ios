//
//  DesignableTextField.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 31/5/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableTextField: UITextField, UITextFieldDelegate {

    @IBInspectable var leftIcon: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightIcon: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftButtonImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightButtonImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        
        // Left icon
        if let lftIcon = leftIcon {
            
            leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 20, height: 20))
            imageView.image = lftIcon
            
            var width = leftPadding + 20 + rightPadding
            
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
                width = width + 5
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            
            leftView = view
            
        } else {
            
            // Image is nil
            leftViewMode = .never
            
        }
        
        // Right icon
        if let rghtIcon = rightIcon {
            
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 20, height: 20))
            imageView.image = rghtIcon
            
            var width = leftPadding + 20
            
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
                width = width + 5
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            
            rightView = view
            
        } else {
            
            // Image is nil
            rightViewMode = .never
            
        }
        
        // Change placeholder text colour
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "", attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        // Change border
        // ISSUE: Does not show up when run; only shows up in storyboard
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }

}
