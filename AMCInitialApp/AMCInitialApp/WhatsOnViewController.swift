//
//  WhatsOnViewController.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 31/5/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

enum MenuType {
    
    case whatsOn
    case foodBeverage
    
}

class WhatsOnViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var whatsOnScrollView: UIScrollView!
    @IBOutlet weak var whatsOnPageControl: UIPageControl!
    @IBOutlet weak var toggleMenuButton: UIBarButtonItem!
    
    var postIndexToShow: Int = 0
    
    // Hardcoded menuType
    var menuType: MenuType = MenuType.foodBeverage
    
    // Hardcoded What's On posts
    var whatsOnPosts: [WhatsOnPost] = {
       
        var firstPost = WhatsOnPost()
        firstPost.title = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit."
        firstPost.days = "1"
        firstPost.imageName = "whatsOn1"
        firstPost.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl."
        
        var secondPost = WhatsOnPost()
        secondPost.title = "Morbi in sem quis dui placerat ornare."
        secondPost.days = "3"
        secondPost.imageName = "whatsOn2"
        secondPost.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl."
        
        var thirdPost = WhatsOnPost()
        thirdPost.title = "Phasellus ultrices nulla quis nibh. Quisque a lectus."
        thirdPost.days = "4"
        thirdPost.imageName = "whatsOn3"
        thirdPost.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl."
        
        var fourthPost = WhatsOnPost()
        fourthPost.title = "Fusce lacinia arcu et nulla. Nulla vitae mauris non felis."
        fourthPost.days = "5"
        fourthPost.imageName = "whatsOn4"
        fourthPost.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl."
        
        return [firstPost, secondPost, thirdPost, fourthPost]
        
    }()
    
    // Hardcoded Restaurants (NEED TO SET PROMOTIONS)
    var restaurants: [Restaurant] = {
        
        var firstRestaurant = Restaurant()
        firstRestaurant.title = "Marina Grill"
        firstRestaurant.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus."
        firstRestaurant.imageName = "rest1"
        firstRestaurant.categories = "Steak, Seafood"
        
        firstRestaurant.featuredPromotions = {
            
            var firstPromotion = Promotion()
            firstPromotion.title = "30% OFF at the Marina Grill!"
            firstPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            firstPromotion.imageName = "restpromo1"
            
            var secondPromotion = Promotion()
            secondPromotion.title = "UPTO 20% OFF on selected wines!"
            secondPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            secondPromotion.imageName = "restpromo2"
            
            var thirdPromotion = Promotion()
            thirdPromotion.title = "30% OFF on dessert! Limited time only."
            thirdPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            thirdPromotion.imageName = "restpromo3"
            
            return [firstPromotion, secondPromotion, thirdPromotion]
        }()
        
        var secondRestaurant = Restaurant()
        secondRestaurant.title = "The Bar"
        secondRestaurant.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus."
        secondRestaurant.imageName = "rest2"
        secondRestaurant.categories = "Drinks, Refreshments"
        
        secondRestaurant.featuredPromotions = {
            
            var firstPromotion = Promotion()
            firstPromotion.title = "30% OFF at the Bar!"
            firstPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            firstPromotion.imageName = "restpromo1"
            
            var secondPromotion = Promotion()
            secondPromotion.title = "UPTO 20% OFF on selected wines!"
            secondPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            secondPromotion.imageName = "restpromo2"
            
            var thirdPromotion = Promotion()
            thirdPromotion.title = "30% OFF on dessert! Limited time only."
            thirdPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            thirdPromotion.imageName = "restpromo3"
            
            return [firstPromotion, secondPromotion, thirdPromotion]
        }()
        
        var thirdRestaurant = Restaurant()
        thirdRestaurant.title = "The Deck"
        thirdRestaurant.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus."
        thirdRestaurant.imageName = "rest3"
        thirdRestaurant.categories = "Barbecue, Seafood"
        
        thirdRestaurant.featuredPromotions = {
            
            var firstPromotion = Promotion()
            firstPromotion.title = "30% OFF at the Deck!"
            firstPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            firstPromotion.imageName = "restpromo1"
            
            var secondPromotion = Promotion()
            secondPromotion.title = "UPTO 20% OFF on selected wines!"
            secondPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            secondPromotion.imageName = "restpromo2"
            
            var thirdPromotion = Promotion()
            thirdPromotion.title = "30% OFF on dessert! Limited time only."
            thirdPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            thirdPromotion.imageName = "restpromo3"
            
            return [firstPromotion, secondPromotion, thirdPromotion]
        }()
        
        var fourthRestaurant = Restaurant()
        fourthRestaurant.title = "The Horizon"
        fourthRestaurant.desc = "Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. \n\nSed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus."
        fourthRestaurant.imageName = "rest4"
        fourthRestaurant.categories = "Fine Dining, Family Style"
        
        fourthRestaurant.featuredPromotions = {
            
            var firstPromotion = Promotion()
            firstPromotion.title = "30% OFF at the Horizon!"
            firstPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            firstPromotion.imageName = "restpromo1"
            
            var secondPromotion = Promotion()
            secondPromotion.title = "UPTO 20% OFF on selected wines!"
            secondPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            secondPromotion.imageName = "restpromo2"
            
            var thirdPromotion = Promotion()
            thirdPromotion.title = "30% OFF on dessert! Limited time only."
            thirdPromotion.desc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna. \n\nDonec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis."
            thirdPromotion.imageName = "restpromo3"
            
            return [firstPromotion, secondPromotion, thirdPromotion]
        }()
        
        return [firstRestaurant, secondRestaurant, thirdRestaurant, fourthRestaurant]
        
    }()

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Set-up UIScrollView
        whatsOnScrollView.isPagingEnabled = true
        whatsOnScrollView.showsHorizontalScrollIndicator = false
        whatsOnScrollView.delegate = self
        
        toggleMenuButton.target = self.revealViewController()
        toggleMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        switch menuType {
        case MenuType.whatsOn:
            whatsOnPageControl.numberOfPages = whatsOnPosts.count
            whatsOnScrollView.contentSize = CGSize(width: self.view.bounds.width * CGFloat(whatsOnPosts.count), height: 603)
            self.navigationItem.title = "What's On"
            loadWhatsOnPosts()
        case MenuType.foodBeverage:
            whatsOnPageControl.numberOfPages = restaurants.count
            whatsOnScrollView.contentSize = CGSize(width: self.view.bounds.width * CGFloat(restaurants.count), height: 603)
            self.navigationItem.title = "Food & Beverage"
            loadRestaurants()
        }
        
    }
    
    // Load What's On posts onto UIScrollView
    func loadWhatsOnPosts() {
        
        for (index, post) in whatsOnPosts.enumerated() {
            
            if let postView = Bundle.main.loadNibNamed("WhatsOnPost", owner: self, options: nil)?.first as? WhatsOnPostView {
                
                postView.backgroundImage.image = UIImage(named: post.imageName!)
                postView.titleLabel.text = post.title
                
                var daysAgoText = "0 days ago"
                
                if let daysAgo = post.days {
                    
                    let days: Int? = Int(daysAgo)
                    
                    if days != 1 {
                        daysAgoText = post.days! + " days ago"
                    } else {
                        daysAgoText = post.days! + " day ago"
                    }
                    
                }
                
                postView.daysAgoLabel.text = daysAgoText
                
                whatsOnScrollView.addSubview(postView)
                postView.frame.size.width = self.view.bounds.size.width
                postView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
                
                postView.readPostButton.tag = index
                postView.readPostButton.addTarget(self, action: #selector(WhatsOnViewController.showPostDetails(_:)), for: .touchUpInside)
                
            }
            
        }
        
    }
    
    // Load Restaurants onto UIScrollView
    func loadRestaurants() {
        
        for (index, restaurant) in restaurants.enumerated() {
            
            if let restaurantView = Bundle.main.loadNibNamed("Restaurant", owner: self, options: nil)?.first as? RestaurantView {
                
                restaurantView.backgroundImage.image = UIImage(named: restaurant.imageName!)
                restaurantView.titleLabel.text = restaurant.title
                restaurantView.categoriesLabel.text = restaurant.categories
                
                whatsOnScrollView.addSubview(restaurantView)
                restaurantView.frame.size.width = self.view.bounds.size.width
                restaurantView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
                
                restaurantView.getRestaurantInfoButton.tag = index
                restaurantView.getRestaurantInfoButton.addTarget(self, action: #selector(WhatsOnViewController.showPostDetails(_:)), for: .touchUpInside)
                
            }
            
        }
        
    }
    
    // UIScrollView Paging
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        whatsOnPageControl.currentPage = Int(page)
        
    }
    
    // Show post details
    func showPostDetails(_ sender: UIButton) {
        
        postIndexToShow = sender.tag
        
        switch menuType {
        case MenuType.whatsOn:
            self.performSegue(withIdentifier: "whatsOnDetailSegue", sender: self)
        case MenuType.foodBeverage:
            // CHANGE THIS
            self.performSegue(withIdentifier: "restaurantSegue", sender: self)
        }
        
    }
    
    // Pass post data to WhatsOnDetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch menuType {
        case MenuType.whatsOn:
            let destination: WhatsOnDetailViewController = segue.destination as! WhatsOnDetailViewController
            destination.whatsNewPost = whatsOnPosts[postIndexToShow]
            destination.postType = menuType
        case MenuType.foodBeverage:
            // CHANGE THIS
            let destination: RestaurantViewController = segue.destination as! RestaurantViewController
            destination.restaurant = restaurants[postIndexToShow]
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
