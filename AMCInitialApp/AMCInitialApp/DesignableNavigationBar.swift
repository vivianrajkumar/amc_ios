//
//  DesignableNavigationBar.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 31/5/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableNavigationBar: UINavigationBar {
    
    @IBInspectable var bgColor: UIColor? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var iconColor: UIColor? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var textColor: UIColor? {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        
        // Set background color
        if let bg = bgColor {
            
            barTintColor = bg
            
        }
        
        // Set icon color
        if let iconCol = iconColor {
            
            tintColor = iconCol
            
        }
        
        // Set text color
        if let textCol = textColor {
            
            titleTextAttributes = [NSForegroundColorAttributeName:textCol]
            
        }
        
        
        isTranslucent = false
        
    }

}
