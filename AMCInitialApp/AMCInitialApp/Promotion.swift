//
//  Promotion.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 7/6/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import Foundation

class Promotion: NSObject {
    
    var imageName: String?
    var title: String?
    var desc: String?
    
}
