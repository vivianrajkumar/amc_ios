//
//  CustomMenuTableViewCell.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 1/6/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import UIKit

class CustomMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
