//
//  Restaurant.swift
//  AMCInitialApp
//
//  Created by Vivian Rajkumar on 6/6/2017.
//  Copyright © 2017 VivianRajkumar. All rights reserved.
//

import Foundation

class Restaurant: NSObject {
    
    var imageName: String?
    var title: String?
    var desc: String?
    var categories: String?
    var featuredPromotions: [Promotion]?
    var otherPromotions: [Promotion]?
    
}
